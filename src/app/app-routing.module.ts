import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {//Si entra a auth carga el localChildren
    path: 'auth',
    //Cargar las rutas hijas, se cargan mediante el modulo, relacionado a los componentes
    loadChildren: () => import ('./auth/auth.module').then(m => m.AuthModule),
  },
  { //si entra a productos cargan el loadChildren
    path: 'productos',
    //Cargará las rutas hijas, mediante el modulo relacionado a los componentes
    loadChildren: () => import ('./productos/productos.module').then(m => m.ProductosModule),
  },
  {
    path: '**',
    redirectTo: 'auth'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
